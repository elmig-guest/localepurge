# translation of localepurge_0.7.4_de.po to German
# Translators, if you are not familiar with the PO format, gettext
# documentation is worth reading, especially sections dedicated to
# this format, e.g. by running:
# info -n '(gettext)PO Files'
# info -n '(gettext)Header Entry'
# Some information specific to po-debconf are available at
# /usr/share/doc/po-debconf/README-trans
# or http://www.debian.org/intl/l10n/po-debconf/README-trans#
# Developers do not need to manually edit POT or PO files.
# Erik Schanze <mail@erikschanze.de>, 2004, 2005.
# Erik Schanze <schanzi_@gmx.de>, 2006.
# Frank Stähr <der-storch-85@gmx.net>, 2013.
msgid ""
msgstr ""
"Project-Id-Version: localepurge_0.7.4_de\n"
"Report-Msgid-Bugs-To: localepurge@packages.debian.org\n"
"POT-Creation-Date: 2013-11-08 07:01+0100\n"
"PO-Revision-Date: 2013-11-12 12:16+0200\n"
"Last-Translator: Frank Stähr <der-storch-85@gmx.net>\n"
"Language-Team: German <de@li.org>\n"
"Language: de\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"
"X-Generator: Virtaal 0.7.1\n"

#. Type: multiselect
#. Description
#: ../localepurge.templates:2001
msgid "Locale files to keep on this system:"
msgstr "Locale-Dateien, die auf diesem System verbleiben:"

#. Type: multiselect
#. Description
#: ../localepurge.templates:2001
msgid ""
"The localepurge package will remove all locale files from the system except "
"those that you select here."
msgstr ""
"Das Paket »localepurge« wird alle Locale-Dateien außer denen, die Sie hier "
"auswählen, vom System entfernen."

#. Type: multiselect
#. Description
#: ../localepurge.templates:2001
msgid ""
"When selecting the locale corresponding to your language and country code "
"(such as \"de_DE\", \"de_CH\", \"it_IT\", etc.) it is recommended to choose "
"the two-character entry (\"de\", \"it\", etc.) as well."
msgstr ""
"Beim Auswählen der zu Ihrer Sprache und Ihrem Ländercode gehörenden Locale "
"(wie etwa »de_DE«, »de_CH«, »it_IT« etc.) empfiehlt es sich, auch den Zwei-"
"Buchstaben-Eintrag (»de«, »it« etc.) zu wählen."

#. Type: multiselect
#. Description
#: ../localepurge.templates:2001
msgid ""
"Entries from /etc/locale.gen will be preselected if no prior configuration "
"has been successfully completed."
msgstr ""
"Einträge aus /etc/locale.gen werden vorausgewählt, falls noch keine frühere "
"Konfiguration erfolgreich abgeschlossen wurde."

#. Type: boolean
#. Description
#: ../localepurge.templates:3001
msgid "Use dpkg --path-exclude?"
msgstr "Benutze dpkg --path-exclude?"

#. Type: boolean
#. Description
#: ../localepurge.templates:3001
msgid ""
"dpkg supports --path-exclude and --path-include options to filter files from "
"packages being installed."
msgstr ""
"»dpkg« unterstützt die Optionen --path-exclude und --path-include, um "
"Dateien aus installierten Paketen zu filtern."

#. Type: boolean
#. Description
#: ../localepurge.templates:3001
msgid ""
"Please see /usr/share/doc/localepurge/README.dpkg-path for more information "
"about this feature. It can be enabled (or disabled) later by running \"dpkg-"
"reconfigure localepurge\"."
msgstr ""
"Siehe /usr/share/doc/localepurge/README.dpkg-path für weitere Informationen "
"zu dieser Funktionalität. Es kann mittels »dpkg-reconfigure localepurge« "
"nachträglich aktiviert (oder deaktiviert) werden."

#. Type: boolean
#. Description
#: ../localepurge.templates:3001
msgid ""
"This option will become active for packages unpacked after localepurge has "
"been (re)configured. Packages installed or upgraded together with "
"localepurge may (or may not) be subject to the previous configuration of "
"localepurge."
msgstr ""
"Diese Option wird für Pakete aktiv, die nach der (erneuten) Konfiguration "
"von Localepurge entpackt werden. Pakete, deren Installation oder Upgrade "
"zusammen mit Localepurge erfolgt, könnten der vorherigen Konfiguration von "
"Localepurge unterliegen (oder nicht)."

#. Type: boolean
#. Description
#: ../localepurge.templates:4001
msgid "Really remove all locales?"
msgstr "Wirklich alle Locales entfernen?"

#. Type: boolean
#. Description
#: ../localepurge.templates:4001
msgid ""
"No locale has been chosen for being kept. This means that all locales will "
"be removed from this system. Please confirm whether this is really your "
"intent."
msgstr ""
"Es sollen keine Locales behalten werden. Das bedeutet, dass alle Locales von "
"diesem System entfernt werden. Bitte bestätigen Sie, dass Sie das wirklich "
"wollen."

#. Type: note
#. Description
#: ../localepurge.templates:5001
msgid "No localepurge action until the package is configured"
msgstr "Keine Aktionen von Localepurge, bis das Paket konfiguriert ist"

#. Type: note
#. Description
#: ../localepurge.templates:5001
msgid ""
"The localepurge package will not be useful until it has been successfully "
"configured using the command \"dpkg-reconfigure localepurge\". The "
"configured entries from /etc/locale.gen of the locales package will then be "
"automatically preselected."
msgstr ""
"Das Paket »localepurge« ist nicht nützlich, bis es erfolgreich mit dem "
"Befehl »dpkg-reconfigure localepurge« eingerichtet worden ist. Die "
"konfigurierten Einträge aus /etc/locale.gen des Pakets »locales« werden dann "
"automatisch vorausgewählt."

#. Type: boolean
#. Description
#: ../localepurge.templates:6001
msgid "Also delete localized man pages?"
msgstr "Übersetzte Handbuchseiten auch löschen?"

#. Type: boolean
#. Description
#: ../localepurge.templates:6001
msgid ""
"Based on the same locale information you chose, localepurge can also delete "
"localized man pages."
msgstr ""
"Aufgrund der von Ihnen ausgewählten Landeskennung kann Localepurge auch "
"übersetzte Handbuchseiten entfernen."

#. Type: boolean
#. Description
#: ../localepurge.templates:7001
msgid "Inform about new locales?"
msgstr "Neue Locales melden?"

#. Type: boolean
#. Description
#: ../localepurge.templates:7001
msgid ""
"If you choose this option, you will be given the opportunity to decide "
"whether to keep or delete newly introduced locales."
msgstr ""
"Wenn Sie diese Option wählen, werden Sie die Möglichkeit erhalten, zu "
"entscheiden, ob neu eingeführte Locales behalten oder gelöscht werden sollen."

#. Type: boolean
#. Description
#: ../localepurge.templates:7001
msgid ""
"If you don't choose it, newly introduced locales will be automatically "
"dropped from the system."
msgstr ""
"Wenn Sie sie nicht wählen, werden neu eingeführte Locales vom System "
"automatisch entfernt."

#. Type: boolean
#. Description
#: ../localepurge.templates:8001
msgid "Display freed disk space?"
msgstr "Freigewordenen Festplattenplatz anzeigen?"

#. Type: boolean
#. Description
#: ../localepurge.templates:8001
msgid ""
"The localepurge program can display the disk space freed by each operation, "
"and show a final summary of saved disk space."
msgstr ""
"Das Programm Localepurge kann den freigewordenen Festplattenplatz bei jeder "
"Operation sowie eine abschließende Zusammenfassung der gesparten "
"Speicherkapazitäten anzeigen."

#. Type: boolean
#. Description
#: ../localepurge.templates:9001
msgid "Accurate disk space calculation?"
msgstr "Genaue Berechnung des Festplattenplatzes?"

#. Type: boolean
#. Description
#: ../localepurge.templates:9001
msgid ""
"There are two ways available to calculate freed disk space. One is much "
"faster than the other but far less accurate if other changes occur on the "
"file system during disk space calculation. The other one is more accurate "
"but slower."
msgstr ""
"Es gibt zwei Arten, den freigewordenen Festplattenplatz zu berechnen. Die "
"eine ist viel schneller als die andere, aber deutlich ungenauer, falls "
"während der Berechnung weitere Änderungen am Dateisystem stattfinden. Die "
"zweite Methode ist genauer, aber langsamer."

#. Type: boolean
#. Description
#: ../localepurge.templates:10001
msgid "Display verbose output?"
msgstr "Ausführliche Ausgaben anzeigen?"

#. Type: boolean
#. Description
#: ../localepurge.templates:10001
msgid ""
"The localepurge program can be configured to explicitly show which locale "
"files it deletes. This may cause a lot of screen output."
msgstr ""
"Das Programm Localepurge kann so eingerichtet werden, dass es explizit "
"anzeigt, welche Locale-Dateien es löscht. Dies könnte viele "
"Bildschirmausgaben nach sich ziehen."

#~ msgid "Selecting locale files"
#~ msgstr "Locale-Dateien auswählen:"

#~ msgid ""
#~ "localepurge will remove all locale files from your system but the ones "
#~ "for the language codes you select now. Usually two character locales like "
#~ "\"de\" or \"pt\" rather than \"de_DE\" or \"pt_BR\" contain the major "
#~ "portion of localizations. So please select both for best support of your "
#~ "national language settings.  The entries from /etc/locale.gen will be "
#~ "preselected if no prior configuration has been successfully completed."
#~ msgstr ""
#~ "Localepurge entfernt alle Locale-Dateien von Ihrem System, außer jenen, "
#~ "deren Länderkennung Sie hier auswählen. Normalerweise enthalten die "
#~ "zweibuchstabigen Locales wie \"de\" oder \"pt\" anstelle von \"de_DE\" "
#~ "oder \"pt_BR\" den Hauptanteil der Lokalisierungen. Wählen Sie also beide "
#~ "Varianten aus, damit Sie die bestmögliche Unterstützung für Ihre "
#~ "nationalen Spracheinstellungen erhalten."

#~ msgid "localepurge will not take any action"
#~ msgstr "Localepurge wird keine Änderungen vornehmen"

#~ msgid ""
#~ "If you are content with the selection of locales you chose to keep and "
#~ "don't want to care about whether to delete or keep newly found locales, "
#~ "just deselect this option to automatically remove new locales you "
#~ "probably wouldn't care about anyway. If you select this option, you will "
#~ "be given the opportunity to decide whether to keep or delete newly "
#~ "introduced locales."
#~ msgstr ""
#~ "Wenn Sie die Auswahl der Locales so erhalten möchten, wie Sie es "
#~ "ausgewählt haben, und es Ihnen egal ist, ob neu gefundene Locales "
#~ "gelöscht oder behalten werden, lehnen Sie diese Option ab, um neue "
#~ "Locales automatisch zu löschen, die Sie ohnehin nicht interessieren. Wenn "
#~ "Sie hier zustimmen, haben Sie die Möglichkeit zu entscheiden, ob Sie neu "
#~ "eingeführte Locales behalten oder löschen wollen."
